<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$app = require_once __DIR__ . '/../app.php';

$app->run();