<?php

require_once __DIR__ . '/vendor/autoload.php';

$app = new Silex\Application();
if ( '127.0.0.1' == $_SERVER['REMOTE_ADDR'] ) {
  $app['debug'] = 'true';
}

// Config
$app['config'] = $app->share(function () use ($app) {
  $data = parse_ini_file( __DIR__ . '/config/app.ini', true );
  return $data;
});

// ActiveRecord
\ActiveRecord\Config::initialize(function() use($app) {
  \ActiveRecord\Config::instance()->set_model_directory(__DIR__ . '/src/Models');
  \ActiveRecord\Config::instance()->set_connections(array(
    'production' => $app['config']['db']['dsn']
  ));
  \ActiveRecord\Config::instance()->set_default_connection('production');
});

// View
require_once __DIR__ . '/src/View.php';
$app['view'] = $app->share(function () use($app) {
  return new View($app);
});

// Controller
$app->mount('/', new \Controllers\PlayList());

return $app;