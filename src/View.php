<?php

class View {
  private $app = null;

  public function __construct($app) {
    $this->app = $app;
  }

  public function render($layout, $template, $vars = array()) {
    $path = __DIR__ . '/../views';

    extract($vars);

    ob_start();
    require_once $path . '/' . $template;

    $content = ob_get_clean();
    if (null == $layout) {
      return $content;
    }

    ob_start();
    require_once $path . '/' . $layout;
    $html = ob_get_clean();
    return $html;
  }
}