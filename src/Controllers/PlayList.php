<?php

namespace Controllers;

use Models\Tracks;
use Silex\Application;
use Silex\ControllerProviderInterface;

class PlayList Implements ControllerProviderInterface {

  public function connect(Application $app) {
    $index = $app['controllers_factory'];

    $index->get('/', function() use($app) {
      return $app['view']->render('layout.phtml', 'playlist.phtml', array(
        'songs' => Tracks::all()
      ));
    });

    $index->post('/', function() use($app) {
      return 'post';
    });

    $index->put('/', function() use($app) {
      return 'put';
    });

    $index->delete('/', function() use($app) {
      return 'delete';
    });

    return $index;
  }
}